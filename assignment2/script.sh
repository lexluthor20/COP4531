#!/bin/bash

rm linear_probe.txt
./linear_plot
rm double_probe.txt
./double_plot
rm part1_probe.txt
./part1_plot
rm quadratic_probe.txt
./quadratic_plot
python stats.py
