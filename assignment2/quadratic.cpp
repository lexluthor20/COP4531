#include "HTABLE.h"
#include <cmath>
#include <fstream>

//void probe_count(int *, int);
//int hash(int *, int, int, int *);

//initialize an array of size 1000000
void init(int * hash_table)
{
    for(int i = 0; i < HT_SIZE; i++)
        hash_table[i] = 0;
}

//inserting val into hash_table
void insert(int * hash_table, int val)
{
    int dummy = 0;
    int index = hash(hash_table,val,0,&dummy);

    if(index != -1)
        hash_table[index] = val;
}
//inserting into hash_table and writing number of probes it took to insert to file
void probe_count(int * hash_table, int val)
{
    int count = 0;
    int index = hash(hash_table,val,0,&count);

    if(index != -1)
        hash_table[index] = val;

    std::fstream fs;
    fs.open("quadratic_probe.txt", std::fstream::in|std::fstream::out|std::fstream::app);
    fs << count << "\n";
    fs.close();
}
//Returns 1 if val is in hash_table, 0 otherwise
char search(int * hash_table, int val)
{
    int dummy = 0;
    int index = hash(hash_table,val,val,&dummy);
    if(index == -1)
        return 0;
    else
        return 1;
}
//clears entry in hash table where val is stored
void remove(int * hash_table, int val)
{
    int dummy = 0;
    int index = hash(hash_table,val,val,&dummy);

    if(index != -1)
        hash_table[index] = 0;
}
int hash(int * hash_table, int val, int vORz, int * probe_count)
{
    if((*probe_count) == 0)
        (*probe_count)++;

    const int key = abs(val) % HT_SIZE;
    int probe = key;
    int i = 0;
    float c1 = 0.5;
    float c2 = 0.5;
    int count = 0;
    //check hash_table for val
    while(hash_table[probe % HT_SIZE] != vORz)
    {
        probe = key + static_cast<int>(c1*i + c2*pow(i,2));
        //returns if it isn't in hash table
        if(count >= HT_SIZE)
            return -1;

        count++;
        i++;
        (*probe_count)++;
    }
    return (probe % HT_SIZE);
}

