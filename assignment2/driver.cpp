#include <iostream>
#include "HTABLE.h"
using namespace std;

void print_table(int *, int);
void print_search(int, int);

int main()
{
    const int size = HT_SIZE;
    int hash_table[size];
    int a,b,c,d,e,f,g,h,i,j,k;
    //a=12,b=24,c=36,d=48,e=60,f=72,g=84,h=96,i=108,j=120;
    a=10,b=20,c=30,d=40,e=60,f=70,g=80,h=90,i=100,j=120;
    init(hash_table);
    print_table(hash_table,size);

    cout << "Inserting " << a << endl;
    insert(hash_table, a);
    print_table(hash_table,size);

    cout << "Inserting " << b << endl;
    insert(hash_table, b);
    print_table(hash_table,size);

    cout << "Inserting " << c << endl;
    insert(hash_table, c);
    print_table(hash_table,size);

    cout << "Inserting " << d << endl;
    insert(hash_table, d);
    print_table(hash_table,size);

    cout << "Inserting " << e << endl;
    insert(hash_table, e);
    print_table(hash_table,size);

    cout << "Inserting " << f << endl;
    insert(hash_table, f);
    print_table(hash_table,size);

    cout << "Inserting " << g << endl;
    insert(hash_table, g);
    print_table(hash_table,size);

    cout << "Inserting " << h << endl;
    insert(hash_table, h);
    print_table(hash_table,size);

    cout << "Inserting " << i << endl;
    insert(hash_table, i);
    print_table(hash_table,size);

    cout << "Inserting " << j << endl;
    insert(hash_table, j);
    print_table(hash_table,size);

    print_search(a,search(hash_table,a));
    print_search(b,search(hash_table,b));
    print_search(c,search(hash_table,c));
    print_search(d,search(hash_table,d));
    print_search(e,search(hash_table,e));
    print_search(f,search(hash_table,f));
    print_search(90,search(hash_table,90));

    cout << "Deleting " << a << endl;
    remove(hash_table,a);
    print_table(hash_table, size);

    cout << "Deleting " << b << endl;
    remove(hash_table,b);
    print_table(hash_table, size);

    cout << "Deleting " << c << endl;
    remove(hash_table,c);
    print_table(hash_table, size);

    cout << "Deleting " << d << endl;
    remove(hash_table,d);
    print_table(hash_table, size);

    cout << "Deleting " << e << endl;
    remove(hash_table,e);
    print_table(hash_table, size);

    cout << "Deleting " << f << endl;
    remove(hash_table,f);
    print_table(hash_table, size);

    return 1;
}

void print_table(int * table, int size)
{
    for(int i = 0; i < size-1; i++)
        cout << table[i] << ", ";
    cout << table[size-1];
    cout << endl;
}

void print_search(int val, int found)
{
    if(found == 1)
        cout << "Found " << val << "\n";
    else
        cout << "Unable to find " << val << "\n";
}
