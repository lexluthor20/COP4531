#ifndef HT_SIZE
#define HT_SIZE 1048576
//#define HT_SIZE 10

void init(int *);
void insert(int *, int);
char search(int *, int);
void remove(int *, int);
void probe_count(int *, int);
int hash(int *, int, int, int *);

#endif
