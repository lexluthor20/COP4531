#include "HTABLE.h"
#include <fstream>

//void probe_count(int *, int);
//int hash(int *, int, int, int *);

//initialize an integer array of size 1000000
void init(int * hash_table)
{
    for(int i = 0; i < HT_SIZE; i++)
        hash_table[i] = 0;
}
//inserting val into hash_table
void insert(int * hash_table, int val)
{
    int trash = 0;
    int index = hash(hash_table,val,0,&trash);

    if(index != -1)
        hash_table[index] = val;
}
void probe_count(int * hash_table, int val)
{
    int count = 0;
    int index = hash(hash_table,val,0,&count);

    if(index != -1)
        hash_table[index] = val;

    std::fstream fs;
    fs.open("double_probe.txt", std::fstream::in|std::fstream::out|std::fstream::app);
    fs << count << "\n";
    fs.close();
}

//returns 1 if val is in hash_table, 0 otherwise
char search(int * hash_table, int val)
{
    int trash = 0;
    int index = hash(hash_table,val,val,&trash);

    if(index == -1)
        return 0;
    else
        return 1;
}
//clears entry in hash_table where val is stored
void remove(int * hash_table, int val)
{
    int trash = 0;
    int index = hash(hash_table,val,val,&trash);

    if(index != -1)
        hash_table[index] = 0;
}
int hash(int * hash_table, int val, int vORz, int * probe_count)
{
    if((*probe_count) == 0)
        (*probe_count)++;

    int i = -1;
    int h1 = abs(val) % HT_SIZE;
    int h2 = 1 + (abs(val) % (HT_SIZE-1));

    if(hash_table[h1] != vORz)
    {
        do
        {
            if(i > HT_SIZE)
                return -1;

            i++;
            (*probe_count)++;

        } while(hash_table[(h1 + i*h2) % HT_SIZE] != vORz);

        return ((h1 + i*h2) % HT_SIZE);
    }
    else
        return h1;
}
