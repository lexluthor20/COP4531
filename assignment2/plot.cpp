#include "HTABLE.h"
#include <iostream>
#include <random>
#include <chrono>

using namespace std;

void fill_table(int *, double, int);
void print_table(int *, int);

typedef std::chrono::high_resolution_clock myclock;
myclock::time_point beginning = myclock::now();

myclock::duration d = myclock::now() - beginning;
unsigned seed = d.count();

default_random_engine generator(seed);



int main()
{
    uniform_int_distribution<int> distribution (pow(-2,31),(pow(2,31)-1));
//    int val = distribution(generator);
    int SIZE = pow(2,20);
    int hash_table[SIZE];

    for(double i = 0.5; i < 1; i+=0.01)
    {
        init(hash_table);
        fill_table(hash_table,i,SIZE);
        probe_count(hash_table,distribution(generator));
    }
/*
    init(hash_table);

    fill_table(hash_table,0.5,HT_SIZE);
//    probe_count(hash_table, val);
    probe_count(hash_table, distribution(generator));

    init(hash_table);

    fill_table(hash_table,0.6,HT_SIZE);
//    probe_count(hash_table, val);
    probe_count(hash_table, distribution(generator));

    init(hash_table);

    fill_table(hash_table,0.7,HT_SIZE);
//    probe_count(hash_table, val);
    probe_count(hash_table, distribution(generator));

    init(hash_table);

    fill_table(hash_table,0.8,HT_SIZE);
//    probe_count(hash_table, val);
    probe_count(hash_table, distribution(generator));

    init(hash_table);

    fill_table(hash_table,0.9,HT_SIZE);
//    probe_count(hash_table, val);
    probe_count(hash_table, distribution(generator));

    init(hash_table);

    fill_table(hash_table,0.93,HT_SIZE);
//    probe_count(hash_table, val);
    probe_count(hash_table, distribution(generator));

    init(hash_table);

    fill_table(hash_table,0.95,HT_SIZE);
//    probe_count(hash_table, val);
    probe_count(hash_table, distribution(generator));

    init(hash_table);

    fill_table(hash_table,0.97,HT_SIZE);
//    probe_count(hash_table, val);
    probe_count(hash_table, distribution(generator));

    init(hash_table);

    fill_table(hash_table,0.98,HT_SIZE);
//    probe_count(hash_table, val);
    probe_count(hash_table, distribution(generator));

    init(hash_table);

    fill_table(hash_table,0.99,HT_SIZE);
//    probe_count(hash_table, val);
    probe_count(hash_table, distribution(generator));
*/
    return 1;
}
//fill hash table randomly up to load_factor
void fill_table(int * table, double loadf, int size)
{
    //this will generate numbers uniformly between -2^31 and 2^31-1
    uniform_int_distribution<int> distribution (pow(-2,31),(pow(2,31)-1));
    double load = 0;
    int counter = 0;

    //run until the table is filled up to the right load factor
    while((load < loadf) && (counter < size))
    {
    //    insert(table, distribution(generator));
        insert(table, distribution(generator));
        counter++;
        load = static_cast<double>(counter)/static_cast<double>(size);
    }
}
void print_table(int * table, int size)
{
    for(int i = 0; i < size; i++)
        cout << "hash_table[" << i << "] = " << table[i] << endl;
}
