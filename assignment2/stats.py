import matplotlib.pyplot as plt
import numpy as np
import matplotlib.lines as ln
import matplotlib.axes as ax

load_factor = np.array([0.5,0.51,0.52,0.53,0.54,0.55,0.56,0.57,0.58,0.59,0.6,0.61,0.62,0.63,0.64,0.65,0.66,0.67,0.68,0.69,0.7,0.71,0.72,0.73,0.74,0.75,0.76,0.77,0.78,0.79,0.8,0.81,0.82,0.83,0.84,0.85,0.86,0.87,0.88,0.89,0.9,0.91,0.92,0.93,0.94,0.95,0.96,0.97,0.98,0.99])

best_case = []

for i in load_factor:
    best_case.append(1/(1-i))

def plot(filename,color):
    file = open(filename, 'r')
    num_probes = []
    for i in range(len(load_factor)):
        num_probes.append(file.readline())
    plt.plot(load_factor,num_probes,color,label=(filename.split('_')[0]))
    plt.plot(load_factor,best_case,'om-',label='best case')
    plt.xlabel('Load Factor')
    plt.ylabel('Number of Probes')
    plt.title("Number of Probes vs Load Factor")
    plt.legend(loc = 'upper left')
    plt.show()


#plt.show()

plot('linear_probe.txt','ob-')
plot('double_probe.txt','og-')
plot('part1_probe.txt','or-')
plot('quadratic_probe.txt','oc-')



#plt.show()
