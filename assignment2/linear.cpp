#include "HTABLE.h"
#include <fstream>
#include <cmath>

//void probe_count(int *, int);
//int hash(int *, int, int, int *);

//using namespace std;
//initialize an integer array of size 1000000
void init(int * hash_table)
{
    for(int i = 0; i < HT_SIZE; i++)
        hash_table[i] = 0;
}
//inserting val into hash_table
void insert(int * hash_table, int val)
{
    int dummy = 0;
    int index = hash(hash_table,val,0,&dummy);

    //insert val into index if it isn't full
    if(index != -1)
        hash_table[index] = val;

}
//inserting into hash_table and writing number of probes it took to insert to file
void probe_count(int * hash_table, int val)
{
    int count = 0;
    int index = hash(hash_table,val,0,&count);
    //hash table full
    if(index != -1)
        hash_table[index] = val;

    std::fstream fs;
    fs.open("linear_probe.txt", std::fstream::in|std::fstream::out|std::fstream::app);
    fs << count << "\n";
    fs.close();
}
//Returns 1 if val is in hash_table, 0 otherwise
char search(int * hash_table, int val)
{
    int dummy = 0;
    int index = hash(hash_table,val,val,&dummy);
    //hash table full
    if(index == -1)
        return 0;
    else
        return 1;
}
//clears entry in hash table where val is stored
void remove(int * hash_table, int val)
{
    int dummy = 0;
    int index = hash(hash_table,val,val,&dummy);
    //hash table full
    if(index != -1)
        hash_table[index] = 0;
}
int hash(int * hash_table, int val, int vORz, int * probe_count)
{
    if((*probe_count) == 0)
        (*probe_count)++;

    const int key = abs(val) % HT_SIZE;
    int probe = key;
    //int probe_count = 0;
    //probe until you find an empty spot. Modulo size so it loops to front
    while(hash_table[probe % HT_SIZE] != vORz)
    {
        probe++;
        (*probe_count)++;
        //returns -1 if it has looped the entire hash table
        if(key == (probe % HT_SIZE))
            return -1;
    }
    //returns the entry value
    return (probe % HT_SIZE);
}
