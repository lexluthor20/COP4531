#include <stdlib.h>
#include <stdio.h>

int max(int x, int y)
{
    if(x > y)
    {
        return x;
    }
    else
    {
        return y;
    }
}

int knapsack(unsigned int * weights, unsigned int * values, size_t n, unsigned long weight_limit)
{
    int i,w;
    int c[n+1][weight_limit+1];

    //build table in a bottom up manner
    for(int i = 0; i <= n; i++)
    {
        for(int w = 0; w <= weight_limit; w++)
        {
            if(i == 0 || w == 0)
            {
                c[i][w] = 0;
            }
            else if((i > 0) && (weights[i] > w))
            {
                c[i][w] = c[i-1][w];
            }
            else if((i > 0) && (weights[i] <= w))
            {
                c[i][w] = max(values[i] + c[i-1][w-weights[i]], c[i-1][w]);
            }
        }
    }
    return c[n][weight_limit];
}
int main()
{
    unsigned int val[] = {60, 100, 120};
    unsigned int wt[] = {10, 20, 30};
    int W = 50;
    int n = sizeof(val)/sizeof(val[0]);
    printf("%d",knapsack(wt, val, n, W));
    return 0;
}
