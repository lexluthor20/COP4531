#include <math.h>
#include <utility>
#include <iostream>
using namespace std;

int * heap_sort(int * arr, int size);
void HeapSort(int * arr, int heapsize);
void BuildMaxHeap(int * arr, int heapsize);
void MaxHeapify(int * arr, int i, int heapsize);
int Parent(int i);
int Left(int i);
int Right(int i);

int * heap_sort(int * arr, int size)
{
    HeapSort(arr,size);
    return arr;
}
void HeapSort(int * arr, int heapsize)
{
    int length = heapsize-1;
    BuildMaxHeap(arr,heapsize);
    for(int i = length; i >= 1; i--)
    {
        std::swap(arr[0],arr[i]);
        heapsize--;
        MaxHeapify(arr,0,heapsize);
    }
}
void BuildMaxHeap(int * arr, int heapsize)
{
    for(int i = (heapsize/2)-1; i >= 0; i--)
        MaxHeapify(arr,i,heapsize);
}
void MaxHeapify(int * arr, int i, int heapsize)
{
    int largest,l,r;
    l = Left(i);
    r = Right(i);
    //check within array bounds and if leftchild is greater than parent
    if((l < heapsize) && (arr[l] > arr[i]))
        largest = l;
    else
        largest = i;
    //check within array bounds and if rightchild is greatest
    if((r < heapsize) && (arr[r] > arr[i]) && (arr[r] > arr[largest]))
        largest = r;
    //swap and run recursively
    if(largest != i)
    {
        std::swap(arr[largest],arr[i]);
        MaxHeapify(arr,largest,heapsize);
    }
}
int Parent(int i)
{
    return ceil(static_cast<double>(i)/2) -1;
}
int Left(int i)
{
    return (2*i) + 1;
}
int Right(int i)
{
    return (2*i) + 2;
}
