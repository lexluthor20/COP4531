#include <iostream>
#include <utility>
#include <algorithm>
using namespace std;

int largest_pow(int * arr, int size);
void insertion_sort(std::pair<int, int> * arr, size_t len);
//void counting_sort(std::pair<int, int> * A, size_t size);

int * radix_sort(int * arr, int size)
{
    std::pair<int, int>  list[size];
    int n = sizeof(list) / sizeof(list[0]);
    int max_divide = pow(10,largest_pow(arr, size));

    for(int j = 0; j < size; j++)
    {
        list[j].second = arr[j];
        list[j].first = arr[j]%10;
    }

    stable_sort(list,list+n);


    for(int i = 10; i <= max_divide; i = i*10)
    {
        for(int j = 0; j < size; j++)
        {
            list[j].first = (list[j].first/i)%10;
        }

        stable_sort(list,list+n);
    }
    for(int i = 0; i < size; i++)
        arr[i] = list[i].first;

    return arr;
}

//find the largest power
int largest_pow(int * arr, int size)
{
    int max_pow = 0;
    int temp;
    int temp_pow;

    for(int i = 0; i < size; i++)
    {
        temp = 0;
        temp_pow = arr[i];
        while(temp_pow > 10)
        {
            temp_pow = temp_pow/10;
            temp++;

            if(temp > max_pow)
                max_pow = temp;
        }
    }
    return max_pow;
}
