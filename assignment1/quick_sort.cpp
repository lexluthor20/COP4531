#include<utility>

void quicksort(int * arr, int low, int high);
int partition(int * arr, int low, int high);

int * quick_sort(int * arr, int size)
{
    quicksort(arr, 0, size-1);
    return arr;
}

void quicksort(int * arr, int low, int high)
{
    int part;
    if(low < high)
    {
        part = partition(arr, low, high);
        quicksort(arr, low, part-1);
        quicksort(arr, part+1, high);
    }
}
int partition(int * arr, int low, int high)
{
    int pivot = arr[high];
    int i = low-1;
    for(int j = low; j < high; j++)
    {
        if(arr[j] <= pivot)
        {
            i++;
            std::swap(arr[i],arr[j]);
        }
    }
    std::swap(arr[i+1], arr[high]);
    return i+1;
}
