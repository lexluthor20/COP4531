#include <iostream>
using namespace std;

int * counting_sort(int * A, int size)
{
    int B[size+1];

    //finding the largest number to get the range of the numbers for B array
    int largest = A[0];
    for(int i = 0; i < size; i++)
    {
        if(A[i] > largest)
            largest = A[i];
    }
    largest++;

    int C[largest];

    //initialize the output array to 0
    for(int i = 0; i < largest; i++)
        C[i] = 0;
    //get the number of instances of each number
    for(int i = 0; i < size; i++)
        C[A[i]] = C[A[i]]+1;

    //add the current number and previous number and save to current
    for(int i = 1; i < largest; i++)
        C[i] = C[i] + C[i-1];

    //sort the final array. start from back for stable sort
    for(int i = size-1; i >= 0; i--)
    {
        B[C[A[i]]] = A[i];
        C[A[i]] = C[A[i]] - 1;
    }

    //copying the sorted array back over
    for(int i = size+1; i > 0; i--)
        A[i-1] = B[i];

    return A;
}
