import matplotlib.pyplot as plt
import numpy as np
import matplotlib.lines as ln
import matplotlib.axes as ax

#x = np.linspace(0, 2, 100)

size = np.array([10,50,100,500,1000,5000,10000,50000,100000,500000,1000000])

file = open('runtime.txt','r')

def plot(color):
    time = []
    sort = file.readline()
    #    time.append(int(file.readline()))
    #file.close()
    #x = np.array(time)
    #print(x)
    #print(time)
    #print(size)
    #plt.plot(n,n,'-o', label = sort)
    for i in range(11):
#        x = size[i]
#        y = int(file.readline())
        time.append((float(file.readline())/16))
    plt.plot(size,time,color,label=sort)
'''
    plt.xlabel('size')
    plt.ylabel('cpu ticks')
    plt.title("number of cpu ticks vs input size")
    plt.legend()
    plt.show()
'''
#    x = np.array(size)
#    y = np.array(time)
#    print(x)
#    print(y)
#    ln.Line2D(size,time, 1, '-', 'blue', 'o')
plot('ob-')
plot('og-')
plot('or-')
plot('oc-')
plot('om-')
plot('oy-')
plot('ok-')


#x = np.array([1,2,3])
#print(x)
#m = np.array(x)

#plt.plot(m, m, label = 'linear')
#plt.plot(x, x, label = 'quadratic')
#plt.plot(x, x, label = 'cubic')

plt.xlabel('size')
plt.ylabel('cpu ticks')
plt.title("number of cpu ticks vs input size")

plt.legend()

plt.show()

