#include <stdlib.h>

int * merge_sort(int * A, size_t size);
int * MergeSort(int * A, int low, int high);
void Merge(int * A, int low, int high, int mid);

int * merge_sort(int * A, size_t size)
{
    MergeSort(A,0,size-1);
    return A;
}
int * MergeSort(int * A, int low, int high)
{
    int mid;

    if(low < high)
    {
        //midpoint
        mid = (low+high)/2;
        //recurisvely call MergeSort
        MergeSort(A, low, mid);
        MergeSort(A, mid+1, high);
        //Merge lists back together
        Merge(A, low, high, mid);
    }
    return A;
}
void Merge(int * A, int low, int high, int mid)
{
    int i, j, k;
    int temp[high-low+1]; //temporary array
    i = low;
    k = 0;
    j = mid+1;

    //Merge parts into temp
    //comparing the left side with the right side
    while(i <= mid && j <= high)
    {
        if(A[i] < A[j])
            temp[k++] = A[i++];
        else
            temp[k++] = A[j++];
    }
    //insert remaining values into temp
    while(i <= mid)
        temp[k++] = A[i++];
    while(j <= high)
        temp[k++] = A[j++];
    //copy data from temp to A
    for(i = low; i <= high; i++)
    {
        A[i] = temp[i-low];
    }
}
