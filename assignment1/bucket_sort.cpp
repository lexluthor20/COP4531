#include <list>

double * bucket_sort(double * A, size_t size)
{
    //array of doubly linked lists
    std::list<double> B[10];
    //iterator
    std::list<double>::iterator it;

    for(int i = 0; i < size; i++)
        B[static_cast<int>(10 * A[i])].push_back(A[i]);

    for(int i = 0; i < 10; i++)
    {
        B[i].sort();
    }

    //merge all the lists into B[0]
    for(int i = 1; i < 10; i++)
        B[0].merge(B[i]);

    //copy list back into original array
    it = B[0].begin();
    for(int i = 0; i < 10; i++)
    {
        if(it != B[0].end())
        {
            A[i] = *it;
            it++;
        }
    }

    return A;
}
