#include <stdlib.h>
#include <utility>

int * insertion_sort(int * arr, size_t len)
{
    int i;
    int key;

    for(int j = 1; j < len; j++)
    {
        key = arr[j];
        i = j-1;
        while((i >= 0) && (arr[i] > key))
        {
            arr[i+1] = arr[i];
            i = i-1;
        }
        arr[i+1] = key;
    }
    return arr;
}
