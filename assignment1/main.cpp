#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <fstream>
#include <random>
#include "insertion_sort.cpp"
#include "merge_sort.cpp"
#include "quick_sort.cpp"
#include "heap_sort.cpp"
#include "counting_sort.cpp"
#include "radix_sort.cpp"
#include "bucket_sort.cpp"
#include <iostream>
using namespace std;
void fill_lists(int ***, double ** ,int sort, int input, const int * ins);
void printlist(int *, size_t);
void insertion(int **, fstream *, int input, const int * ins);
void merge(int **, fstream *, int input, const int * ins);
void quick(int **, fstream *, int input, const int * ins);
void heap(int **, fstream *, int input, const int * ins);
void counting(int **, fstream *, int input, const int * ins);
void radix(int **, fstream *, int input, const int * ins);
void bucket(double **, fstream *, int input, const int * ins);

int main()
{
    const int sorts = 7;
    const int inputs = 11;
    const int ins[11] =
                {10,50,100,500,1000,5000,10000,50000,100000,500000,1000000};

    //creating the input lists to test
    int *** list = new int **[sorts];

    for(int i = 0; i < sorts; i++)
        list[i] = new int *[inputs];

    for(int i = 0; i < sorts; i++)
    {
        for(int j = 0; j < inputs; j++)
        {
            list[i][j] = new int[ins[j]];
        }
    }

    double ** dlist = new double *[inputs];
    for(int i = 0; i < inputs; i++)
    {
        dlist[i] = new double[ins[i]];
    }

    srand(time(NULL));
    fill_lists(list,dlist,sorts,inputs,ins);

    //running sorts
    fstream fs;

    insertion(list[0],&fs,inputs,ins);
    merge(list[1],&fs,inputs,ins);
    quick(list[2],&fs,inputs,ins);
    heap(list[3],&fs,inputs,ins);
    counting(list[4],&fs,inputs,ins);
    radix(list[5],&fs,inputs,ins);
    bucket(dlist,&fs,inputs,ins);

//    printf("ORIGINAL LIST\n");
//    print_list(arr, size);
//    insertion_sort(arr, size);
//    merge_sort(arr, size);
//    quick_sort(arr, size);
//    heap_sort(arr, size);
//    counting_sort(arr, size);
//    radix_sort(arr, size);
//    bucket_sort(arr, size);
//    printf("SORTED LIST\n");
//    print_list(arr, size);
}
//fill list randomly
void fill_lists(int *** l,double ** dl,int sort,int input,const int * ins)
{
//    long num = pow(2,16);
    default_random_engine generator;
    uniform_int_distribution<int> distribution (0,(pow(2,16)-1));

    for(int j = 0; j < input; j++)
    {
        for(int i = 0; i < ins[j]; i++)
        {
            l[0][j][i] = distribution(generator);
        }
    }

    //copying the same values to the other 6 sorts
    for(int i = 1; i < sort; i++)
    {
        for(int j = 0; j < input; j++)
        {
            for(int k = 0; k < ins[j]; k++)
            {
                l[i][j][k] = l[0][j][k];
            }
        }
    }
    for(int i = 0; i < input; i++)
    {
        for(int j = 0; j < ins[i]; j++)
        {
            dl[i][j] = static_cast<double>((l[6][i][j])/(pow(2,18)));
        }
    }


/*
    for(int i = 0; i < sort; i++)
    {
        for(int j = 0; j < 49; j++)
            cout << l[i][1][j] << ", ";
        cout << l[i][1][49] << endl << endl << endl;
    }
*/
}
void printlist(int * l, size_t size)
{
    for(int i = 0; i < size-1; i++)
        cout << l[i] << ", ";
    cout << l[size-1] << endl << endl << endl;
}

void insertion(int ** l, fstream * fs, int input, const int * ins)
{
    fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
    (*fs) << "INSERTION_SORT\n";
    fs->close();
    clock_t t;
    for(int i = 0; i < input; i++)
    {
        fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
        t = clock();
        insertion_sort(l[i],ins[i]);
        t = clock() - t;
        (*fs) << t << "\n";
        fs->close();
    }
}
void merge(int ** l, fstream * fs, int input, const int * ins)
{
    fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
    (*fs) << "MERGE_SORT\n";
    fs->close();
    clock_t t;
    for(int i = 0; i < input; i++)
    {
        fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
        t = clock();
        merge_sort(l[i],ins[i]);
        t = clock() - t;
        (*fs) << t << "\n";
        fs->close();
    }
}
void quick(int ** l, fstream * fs, int input, const int * ins)
{
    fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
    (*fs) << "QUICK_SORT\n";
    fs->close();
    clock_t t;
    for(int i = 0; i < input; i++)
    {
        fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
        t = clock();
        quick_sort(l[i],ins[i]);
        t = clock() - t;
        (*fs) << t << "\n";
        fs->close();
    }
}
void heap(int ** l, fstream * fs, int input, const int * ins)
{
    fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
    (*fs) << "HEAP_SORT\n";
    fs->close();
    clock_t t;
    for(int i = 0; i < input; i++)
    {
        fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
        t = clock();
        heap_sort(l[i],ins[i]);
        t = clock() - t;
        (*fs) << t << "\n";
        fs->close();
    }
}
void counting(int ** l, fstream * fs, int input, const int * ins)
{
    fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
    (*fs) << "COUNTING_SORT\n";
    fs->close();
    clock_t t;
    for(int i = 0; i < input; i++)
    {
        fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
        t = clock();
        counting_sort(l[i],ins[i]);
        t = clock() - t;
        (*fs) << t << "\n";
        fs->close();
    }
}
void radix(int ** l, fstream * fs, int input, const int * ins)
{
    fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
    (*fs) << "RADIX_SORT\n";
    fs->close();
    clock_t t;
    for(int i = 0; i < input; i++)
    {
        fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
        t = clock();
        radix_sort(l[i],ins[i]);
        t = clock() - t;
        (*fs) << t << "\n";
        fs->close();
    }
}
void bucket(double ** dl, fstream * fs, int input, const int * ins)
{
    fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
    (*fs) << "BUCKET_SORT\n";
    fs->close();
    clock_t t;
    for(int i = 0; i < input; i++)
    {
        fs->open("runtime.txt", fstream::in|fstream::out|fstream::app);
        t = clock();
        bucket_sort(dl[i],ins[i]);
        t = clock() - t;
        (*fs) << t << "\n";
        fs->close();
    }
}
